import itertools, json, shutil, time, threading, os, urlparse
import mpd
from werkzeug.exceptions import HTTPException, Forbidden
from werkzeug.routing import Map, Rule
from werkzeug.utils import redirect
from werkzeug.wrappers import Request, Response
from werkzeug.wsgi import SharedDataMiddleware
from werkzeug.contrib.sessions import FilesystemSessionStore

# Atomic counter for track names
counter = itertools.count()

session_store = FilesystemSessionStore()

def dump_track(stream, ext):
    """Dump a stream to unique file with given extension."""
    file = str(counter.next()) + '.' + ext
    with open('music/' + file, 'wb') as f:
        print('Writing new track to', file)
        #d = stream.read()
        print('read', len(stream), 'bytes')
        f.write(stream)
        #shutil.copyfileobj(stream, f)
    return file

class MPQ(object):
    def __init__(self):
        self.mpd = mpd.MPDClient()
        self.mpd.connect('localhost', 6600)
        self.mpd_lock = threading.Lock()

        self.url_map = Map([
            Rule('/', endpoint='index_redirect'),
            Rule('/upload/<name>', endpoint='upload_file'),
            Rule('/vote/<id>', endpoint='vote'),
            Rule('/tracks', endpoint='enumerate_tracks'),
            Rule('/rescan', endpoint='mpd_update'),
            Rule('/nowplaying', endpoint='now_playing'),
        ])
        self.votes = {}

    def dispatch_request(self, request):
        sid = request.cookies.get('sid')
        if sid is None:
            request.session = session_store.new()
        else:
            request.session = session_store.get(sid)

        adapter = self.url_map.bind_to_environ(request.environ)
        try:
            endpoint, values = adapter.match()
            response = getattr(self, endpoint)(request, **values)
        except HTTPException, e:
            return e

        if request.session.should_save:
            session_store.save(request.session)
            response.set_cookie('sid', request.session.sid)
        return response

    def index_redirect(self, request):
        return redirect('/static/index.html')

    def now_playing(self, request):
        with self.mpd_lock:
            return Response(json.dumps(self.mpd.currentsong()))

    def mpd_update(self, request):
        with self.mpd_lock:
            self.mpd.update()
            time.sleep(1)
            self.mpd.clear()
            self.mpd.add('/')
        return Response('OK')

    def enumerate_tracks(self, request):
        with self.mpd_lock:
            info = self.mpd.playlistinfo()
        return Response(json.dumps(info))

    def upload_file(self, request, name):
        assert(request.method in ('POST', 'PUT'))
        print('File upload', request.headers.get('content-length'))
        ext = name.rpartition('.')[2]
        file = dump_track(request.data, ext)
        with self.mpd_lock:
            self.mpd.update()
            # hack. Update isn't instant
            time.sleep(1)
            attrs = self.mpd.addid(file)
        print('Added', file, attrs)
        return Response(attrs)

    def vote(self, request, id):
        print(request.session.sid, 'voting for TID', id)
        if id in request.session:
            return Forbidden('You already voted for this track')
        else:
            request.session[id] = True
        with self.mpd_lock:
            trackinfo = self.mpd.playlistid(id)[0]
            try:
                prio = int(trackinfo['prio'])
            except KeyError:
                prio = 0
            prio += 1
            prio = min(prio, 255)
            self.mpd.prioid(prio, trackinfo['id'])
            trackinfo = self.mpd.playlistid(id)[0]
        return Response(repr(trackinfo))

    def wsgi_app(self, environ, start_response):
        request = Request(environ)
        response = self.dispatch_request(request)
        return response(environ, start_response)

    def __call__(self, environ, start_response):
        return self.wsgi_app(environ, start_response)

def create_app(with_static=True):
    app = MPQ()
    app.wsgi_app = SharedDataMiddleware(app.wsgi_app, {
        '/static': os.path.join(os.path.dirname(__file__), 'static')
    })
    return app

if __name__ == '__main__':
    from werkzeug.serving import run_simple
    app = create_app()
    run_simple('0.0.0.0', 8080, app, use_debugger=True, use_reloader=True)
